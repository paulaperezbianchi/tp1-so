import random

x = "muchosRepetidos" #nombre del archivo a crear

with open("test-1", "r") as file:
    allText = file.read()
    words = list(map(str, allText.split()))

f = open(f'{x}.txt', "a")
for i in range(10000):
    f.write(random.choice(words)+"\n")
