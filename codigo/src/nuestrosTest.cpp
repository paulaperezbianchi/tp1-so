#include <vector>
#include "lib/littletest.hpp"
#include "time.h"
#include "../src/ListaAtomica.hpp"
#include "../src/HashMapConcurrente.hpp"
#include "../src/CargarArchivos.hpp"
#include <iostream>
#include <fstream> //Para leeer el archivo

#include <string>
#include <iomanip>    //Para setear la precision de la salid
#include <cassert>    //Para los asseryt
#include <filesystem> // Para obtener los nombres de los archivos


LT_BEGIN_SUITE(TestNuestros)

HashMapConcurrente hM;
hashMapPair hm_pair;
void set_up()
{
}

void tear_down()
{
}
LT_END_SUITE(TestNuestros)
LT_BEGIN_TEST(TestNuestros, corpus1ThreadYMaximoParalelo)
struct timespec begin, end;

clock_gettime(CLOCK_REALTIME, &begin);

cargarMultiplesArchivos(hM, 1, {"data/10milCorpusRandom", "data/10milCorpusRandom", "data/10milCorpusRandom"});
LT_CHECK_EQ(hM.maximoParalelo(1).first, "consideran");
LT_CHECK_EQ(hM.maximoParalelo(1).second, 51);
// LT_CHECK_EQ(hM.claves().size(), 2186);

clock_gettime(CLOCK_REALTIME, &end);
float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec - begin.tv_sec);
printf("time taken : %f sec \n", seconds);

LT_END_TEST(corpus1ThreadYMaximoParalelo)

LT_BEGIN_TEST(TestNuestros, corpus2ThreadYMaximoParalelo)
struct timespec begin, end;

clock_gettime(CLOCK_REALTIME, &begin);
cargarMultiplesArchivos(hM, 2, {"data/10milCorpusRandom", "data/10milCorpusRandom", "data/10milCorpusRandom"});
LT_CHECK_EQ(hM.maximoParalelo(2).first, "consideran");
LT_CHECK_EQ(hM.maximoParalelo(2).second, 51);
// LT_CHECK_EQ(hM.claves().size(), 2186);

clock_gettime(CLOCK_REALTIME, &end);
float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec - begin.tv_sec);
printf("time taken : %f sec \n", seconds);
LT_END_TEST(corpus2ThreadYMaximoParalelo)

LT_BEGIN_TEST(TestNuestros, corpus3ThreadYMaximoParalelo)
struct timespec begin, end;

clock_gettime(CLOCK_REALTIME, &begin);
cargarMultiplesArchivos(hM, 3, {"data/10milCorpusRandom", "data/10milCorpusRandom", "data/10milCorpusRandom"});
LT_CHECK_EQ(hM.maximoParalelo(3).first, "consideran");
LT_CHECK_EQ(hM.maximoParalelo(3).second, 51);
// LT_CHECK_EQ(hM.claves().size(), 2177);

clock_gettime(CLOCK_REALTIME, &end);
float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec - begin.tv_sec);
printf("time taken : %f sec \n", seconds);
LT_END_TEST(corpus3ThreadYMaximoParalelo)

LT_BEGIN_TEST(TestNuestros, corpus1ThreadYMaximoParaleloMedTiempo)
cargarMultiplesArchivos(hM, 1, {"data/10milCorpusRandom"});

struct timespec begin, end;
clock_gettime(CLOCK_REALTIME, &begin);

hm_pair = hM.maximoParalelo(7);

clock_gettime(CLOCK_REALTIME, &end);
float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec - begin.tv_sec);
printf("time taken : %f sec", seconds);
LT_END_TEST(corpus1ThreadYMaximoParaleloMedTiempo)

LT_BEGIN_TEST(TestNuestros, corpus2ThreadYMaximoParaleloMedTiempo)
cargarMultiplesArchivos(hM, 1, {"data/10milCorpusRandom"});
struct timespec begin, end;
clock_gettime(CLOCK_REALTIME, &begin);

hm_pair = hM.maximoParalelo(8);

clock_gettime(CLOCK_REALTIME, &end);
float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec - begin.tv_sec);
printf("time taken : %f sec", seconds);
LT_END_TEST(corpus2ThreadYMaximoParaleloMedTiempo)

LT_BEGIN_TEST(TestNuestros, corpus3ThreadYMaximoParaleloMedTiempo)
cargarMultiplesArchivos(hM, 1, {"data/10milCorpusRandom"});
struct timespec begin, end;
clock_gettime(CLOCK_REALTIME, &begin);

hm_pair = hM.maximoParalelo(9);

clock_gettime(CLOCK_REALTIME, &end);
float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec - begin.tv_sec);
printf("time taken : %f sec", seconds);
LT_END_TEST(corpus3ThreadYMaximoParaleloMedTiempo)

LT_BEGIN_TEST(TestNuestros, mismaClave)
cargarMultiplesArchivos(hM, 1, {"data/mismaClave"});
LT_CHECK_EQ(hM.maximoParalelo(4).first, "kiwi");
LT_CHECK_EQ(hM.maximoParalelo(4).second, 10000);
LT_CHECK_EQ(hM.claves().size(), 1);

struct timespec begin, end;
clock_gettime(CLOCK_REALTIME, &begin);

hashMapPair h = hM.maximoParalelo(4);

clock_gettime(CLOCK_REALTIME, &end);
float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec - begin.tv_sec);
printf("time taken : %f sec", seconds);

LT_CHECK_EQ(h.first, "kiwi");

LT_END_TEST(mismaClave)

LT_BEGIN_TEST(TestNuestros, mismaInicial)
cargarMultiplesArchivos(hM, 1, {"data/mismaInicial"});
LT_CHECK_EQ(hM.maximoParalelo(4).first, "ahacerme");
LT_CHECK_EQ(hM.maximoParalelo(4).second, 17);
// LT_CHECK_EQ(hM.claves().size(), 8579);
LT_END_TEST(mismaInicial)

LT_BEGIN_TEST(TestNuestros, muchosRepetidos)
cargarMultiplesArchivos(hM, 1, {"data/muchosRepetidos"});
LT_CHECK_EQ(hM.maximoParalelo(4).first, "estegosaurio");
LT_CHECK_EQ(hM.maximoParalelo(4).second, 3046);
// LT_CHECK_EQ(hM.claves().size(), 8942);
struct timespec begin, end;
clock_gettime(CLOCK_REALTIME, &begin);

LT_END_TEST(muchosRepetidos)

LT_BEGIN_TEST(TestNuestros, CorpusDuplicado)
struct timespec begin, end;
cargarMultiplesArchivos(hM, 8, {"data/corpus.txt", "data/corpus.txt"});
for (int i = 1; i < 9; i++)
{
    clock_gettime(CLOCK_REALTIME, &begin);
    hM.maximoParalelo(i);
    clock_gettime(CLOCK_REALTIME, &end);
    float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec - begin.tv_sec);
    printf("time taken : %f sec\n", seconds);
}
LT_END_TEST(CorpusDuplicado)

LT_BEGIN_TEST(TestNuestros, Corpus)
struct timespec begin, end;
cargarMultiplesArchivos(hM, 8, {"data/corpus.txt"});
for (int i = 1; i < 9; i++)
{
    clock_gettime(CLOCK_REALTIME, &begin);
    hM.maximoParalelo(i);
    clock_gettime(CLOCK_REALTIME, &end);
    float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec - begin.tv_sec);
    printf("time taken : %f sec \n", seconds);
}
LT_END_TEST(Corpus)

/*
//TEST PESADO
LT_BEGIN_TEST(TestNuestros, unaInicialPorArchivo1archivo8archivos1thread)
    struct timespec begin, end;
    clock_gettime(CLOCK_REALTIME, &begin);

    cargarMultiplesArchivos(hM, 1, {"data/alphaRandomIniciales/alphaRandom1.txt",
                                    "data/alphaRandomIniciales/alphaRandom2.txt",
                                    "data/alphaRandomIniciales/alphaRandom3.txt",
                                    "data/alphaRandomIniciales/alphaRandom4.txt",
                                    "data/alphaRandomIniciales/alphaRandom5.txt",
                                    "data/alphaRandomIniciales/alphaRandom6.txt",
                                    "data/alphaRandomIniciales/alphaRandom7.txt"});

    clock_gettime(CLOCK_REALTIME, &end);
    float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec  - begin.tv_sec);
    printf("time taken : %f sec", seconds);
LT_END_TEST(unaInicialPorArchivo1archivo8archivos1thread)

LT_BEGIN_TEST(TestNuestros, unaInicialPorArchivo1archivo8archivos)
    struct timespec begin, end;
    clock_gettime(CLOCK_REALTIME, &begin);

    cargarMultiplesArchivos(hM, 1, {"data/alphaRandomIniciales/alphaRandomA.txt",
                                    "data/alphaRandomIniciales/alphaRandomB.txt",
                                    "data/alphaRandomIniciales/alphaRandomC.txt",
                                    "data/alphaRandomIniciales/alphaRandomD.txt",
                                    "data/alphaRandomIniciales/alphaRandomE.txt",
                                    "data/alphaRandomIniciales/alphaRandomF.txt",
                                    "data/alphaRandomIniciales/alphaRandomH.txt"});

    clock_gettime(CLOCK_REALTIME, &end);
    float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec  - begin.tv_sec);
    printf("time taken : %f sec", seconds);
LT_END_TEST(unaInicialPorArchivo1archivo8archivos)
*/

LT_BEGIN_TEST(TestNuestros, unaInicialPorArchivo3archivos)
    struct timespec begin, end;
    clock_gettime(CLOCK_REALTIME, &begin);

    cargarMultiplesArchivos(hM, 3, {"data/inicialesMezcladas/inicialesMezcladas1", "data/inicialesMezcladas/inicialesMezcladas2", "data/inicialesMezcladas/inicialesMezcladas3"});

    clock_gettime(CLOCK_REALTIME, &end);
    float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec  - begin.tv_sec);
    printf("time taken : %f sec", seconds);
LT_END_TEST(unaInicialPorArchivo3archivos)

LT_BEGIN_TEST(TestNuestros, unaInicialPorArchivo3archivosSorted)
    struct timespec begin, end;
    clock_gettime(CLOCK_REALTIME, &begin);

      cargarMultiplesArchivos(hM, 3, {"data/inicialesMezcladas/inicialesMezcladasA", "data/inicialesMezcladas/inicialesMezcladasB", "data/inicialesMezcladas/inicialesMezcladasC"});

    clock_gettime(CLOCK_REALTIME, &end);
    float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec  - begin.tv_sec);
    printf("time taken : %f sec", seconds);

LT_END_TEST(unaInicialPorArchivo3archivosSorted)
/*
LT_BEGIN_TEST(TestNuestros, maximoPareleloAlphaSegunThreads)
    struct timespec begin, end;


    cargarMultiplesArchivos(hM, 1, {"data/words_alpha.txt"});
    clock_gettime(CLOCK_REALTIME, &begin);
    hM.maximo();
    clock_gettime(CLOCK_REALTIME, &end);
    float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec  - begin.tv_sec);
    printf("%i threads time taken : %f sec \n", seconds);

    for(int i = 1; i < 9; i++){
        clock_gettime(CLOCK_REALTIME, &begin);
        hM.maximoParalelo(i);
        clock_gettime(CLOCK_REALTIME, &end);
        float seconds = (end.tv_nsec - begin.tv_nsec) / 1000000000.0 + (end.tv_sec  - begin.tv_sec);
        printf("%i threads time taken : %f sec \n", i, seconds);
    }
LT_END_TEST(maximoPareleloAlphaSegunThreads)
*/
LT_BEGIN_TEST(TestNuestros, maximoParaleloUnaInicial)
struct timespec begin, end;

std::vector<std::string> archivosACargar(12, "data/inicialesMezcladas/inicialesMezcladasA");
cargarMultiplesArchivos(hM, 4, archivosACargar);

clock_gettime(CLOCK_REALTIME, &begin);
hM.maximo();
clock_gettime(CLOCK_REALTIME, &end);
float miliseconds = ((end.tv_nsec - begin.tv_nsec) / 1000000.0) + (end.tv_sec - begin.tv_sec) * 1000; 
printf("Maximo no paralelo time taken : %f ms \n", miliseconds);

for (int i = 1; i < 9; i++)
{
    clock_gettime(CLOCK_REALTIME, &begin);
    hM.maximoParalelo(i);
    clock_gettime(CLOCK_REALTIME, &end);
    float miliseconds = ((end.tv_nsec - begin.tv_nsec) / 1000000.0) + (end.tv_sec - begin.tv_sec) * 1000;
    printf("%i threads time taken : %f ms \n", i, miliseconds);
}


LT_END_TEST(maximoParaleloUnaInicial)

/*
LT_BEGIN_TEST(TestNuestros, cargar8ArchivosGrandes)
struct timespec begin, end;

std::vector<std::string> archivosACargar;

for (int i = 1; i < 80; i++)
{
    std::string archivo = "data/alphaRandomMil/alphaRandomMil (" + to_string(i) + ").txt";
    archivosACargar.push_back(archivo);
}

for (int i = 1; i < 9; i++)
{
    std::string archivo = "data/alphaRandom10MilA/alphaRandom10Mil (" + to_string(i) + ").txt";
    archivosACargar.push_back(archivo);
}

HashMapConcurrente nuevoHash42;
clock_gettime(CLOCK_REALTIME, &begin);
for (auto e : archivosACargar)
{
    cargarArchivo(nuevoHash42, e);
}
clock_gettime(CLOCK_REALTIME, &end);
float miliseconds = ((end.tv_nsec - begin.tv_nsec) / 1000000.0) + (end.tv_sec - begin.tv_sec) * 1000;
printf("No paralelo time taken : %f ms \n", miliseconds);

for (int i = 1; i < 9; i++)
{
    HashMapConcurrente nuevoHashMap;
    clock_gettime(CLOCK_REALTIME, &begin);
    cargarMultiplesArchivos(nuevoHashMap, i, archivosACargar);
    clock_gettime(CLOCK_REALTIME, &end);
    float miliseconds = ((end.tv_nsec - begin.tv_nsec) / 1000000.0) + (end.tv_sec - begin.tv_sec) * 1000;
    printf("%i threads time taken : %f ms \n", i, miliseconds);
}

LT_END_TEST(cargar8ArchivosGrandes)

LT_BEGIN_TEST(TestNuestros, cargar80ArchivosChicos)
struct timespec begin, end;

std::vector<std::string> archivosACargar;

for (int i = 1; i < 80; i++)
{
    std::string archivo = "data/alphaRandomMil/alphaRandomMil (" + to_string(i) + ").txt";
    archivosACargar.push_back(archivo);
}

HashMapConcurrente nuevoHash42;
clock_gettime(CLOCK_REALTIME, &begin);
for (auto e : archivosACargar)
{
    cargarArchivo(nuevoHash42, e);
}
clock_gettime(CLOCK_REALTIME, &end);
float miliseconds = ((end.tv_nsec - begin.tv_nsec) / 1000000.0) + (end.tv_sec - begin.tv_sec) * 1000;
printf("No paralelo time taken : %f ms \n", miliseconds);

for (int i = 1; i < 9; i++)
{
    HashMapConcurrente nuevoHash;
    clock_gettime(CLOCK_REALTIME, &begin);
    cargarMultiplesArchivos(nuevoHash, i, archivosACargar);
    clock_gettime(CLOCK_REALTIME, &end);
    float miliseconds = ((end.tv_nsec - begin.tv_nsec) / 1000000.0) + (end.tv_sec - begin.tv_sec) * 1000;
    printf("%i threads time taken : %f ms \n", i, miliseconds);
}

LT_END_TEST(cargar80ArchivosChicos)
*/
    // Ejecutar tests
LT_BEGIN_AUTO_TEST_ENV()
    AUTORUN_TESTS()
LT_END_AUTO_TEST_ENV()