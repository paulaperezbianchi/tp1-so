#ifndef CHM_CPP
#define CHM_CPP

#include <vector>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <thread>
#include <algorithm>

#include "CargarArchivos.hpp"

int cargarArchivo(
    HashMapConcurrente &hashMap,
    std::string filePath
) {
    std::fstream file;
    int cant = 0;
    std::string palabraActual;

    // Abro el archivo.
    file.open(filePath, file.in);
    if (!file.is_open()) {
        std::cerr << "Error al abrir el archivo '" << filePath << "'" << std::endl;
        return -1;
    }
    while (file >> palabraActual) {
        //Completar
        hashMap.incrementar(palabraActual);
        cant++;
    }
    // Cierro el archivo.
    if (!file.eof()) {
        std::cerr << "Error al leer el archivo" << std::endl;
        file.close();
        return -1;
    }
    file.close();
    return cant;
}

void cargarMultiplesNoConcurrente(
    HashMapConcurrente &hashMap,
    std::vector<std::string> filePaths
) {
    for (auto file : filePaths){
        cargarArchivo(hashMap, file);
    }
}
mutex indice;

void thread_carga_archivo(HashMapConcurrente &tabla, 
                        std::vector<std::string> &filePaths, 
                        int &ind_file){
    indice.lock();
    while(ind_file < filePaths.size()){ 
        int ind = ind_file;
        ind_file++; 
        indice.unlock();
        cargarArchivo(tabla, filePaths[ind]);
        indice.lock(); 
    }
    indice.unlock(); 
}
 
void cargarMultiplesArchivos(
    HashMapConcurrente &hashMap,
    unsigned int cantThreads,
    std::vector<std::string> filePaths
) {
    // Completar (Ejercicio 4)
    unsigned int cantArchivos = filePaths.size();

    cantThreads = std::min(cantThreads, cantArchivos);

    std::vector<thread> threads(cantThreads);
    int ind_file = 0; 

    for (auto& th : threads){
            th = std::thread(thread_carga_archivo, ref(hashMap), ref(filePaths), ref(ind_file));
        }

    for (auto& th : threads){
        th.join();
    }
}

#endif
