#ifndef CHM_CPP
#define CHM_CPP

#include <thread>
// alternativamente #include <pthread.h>
#include <iostream>
#include <fstream>

#include "HashMapConcurrente.hpp"

HashMapConcurrente::HashMapConcurrente() {
    for (unsigned int i = 0; i < HashMapConcurrente::cantLetras; i++) {
        tabla[i] = new ListaAtomica<hashMapPair>();
    }
}

unsigned int HashMapConcurrente::hashIndex(std::string clave) {
    return (unsigned int)(clave[0] - 'a');
}

void HashMapConcurrente::incrementar(std::string clave) {
    bool esta = false;
    int index = hashIndex(clave);
    mutexLetras[index].lock();
    for (auto &p : *tabla[index]) {
        if (p.first == clave) {
            p.second++;
            esta = true;
            break;
        }
    }
    if(!esta){
        tabla[index]->insertar(make_pair(clave, 1));
    }
    mutexLetras[index].unlock();
}

std::vector<std::string> HashMapConcurrente::claves() {
    std::vector<std::string> res;
    for (unsigned int index = 0; index < HashMapConcurrente::cantLetras; index++) {
        for (auto &p : *tabla[index]) {
            res.push_back(p.first);
        }
    }
    return res;
}

unsigned int HashMapConcurrente::valor(std::string clave) {
    int res = 0;
    int index = hashIndex(clave);
    for (auto &p : *tabla[index]) {
        if (p.first == clave) {
            res = p.second;
            break;
        }
    }
    return res;
}

hashMapPair HashMapConcurrente::maximo() {
    hashMapPair *max = new hashMapPair();
    max->second = 0;
    for (unsigned int index = 0; index < HashMapConcurrente::cantLetras; index++) {
        mutexLetras[index].lock();
    }
    for (unsigned int index = 0; index < HashMapConcurrente::cantLetras; index++) {
        for (auto &p : *tabla[index]) {
            if (p.second > max->second) {
                max->first = p.first;
                max->second = p.second;
            }
        }
        mutexLetras[index].unlock();
    }

    return *max;
}


void HashMapConcurrente::threadMax (hashMapPair* maximo, int& nextIndex){
    hashMapPair *max = new hashMapPair();
    max->second = 0;
    mutexMax.lock();
    while (nextIndex < HashMapConcurrente::cantLetras) {
        int index = nextIndex;
        nextIndex++;
        mutexMax.unlock();
        for (auto &p : *tabla[index]) {
            if (p.second > max->second) {
                max->first = p.first;
                max->second = p.second;
            }
        }
        mutexLetras[index].unlock();
        mutexMax.lock();
    }
    if (max->second > maximo->second) {
        maximo->first = max->first;
        maximo->second = max->second;
    }
    mutexMax.unlock();
}

hashMapPair HashMapConcurrente::maximoParalelo(unsigned int cant_threads) {
    hashMapPair *max = new hashMapPair();
    max->second = 0;
    vector<std::thread> threads(cant_threads);
    for (unsigned int index = 0; index < HashMapConcurrente::cantLetras; index++) {
        mutexLetras[index].lock();
    }
    int index = 0;
    for (auto &t : threads) {
        t = std::thread(&HashMapConcurrente::threadMax,this,max,ref(index));
    }
    for (auto &t : threads) {
        t.join();
    }
    return *max;
    
}

#endif
